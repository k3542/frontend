import Axios from 'axios'

let urls = {
  development: '',
  production: ''
}

const api = Axios.create({
  baseURL: urls.development,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

api.interceptors.request.use(
  (config) => {
    const token = window.localStorage.getItem('token')

    if (token) {
      config.headers.Authorization = token
    }

    config.headers.Timezone = Intl.DateTimeFormat().resolvedOptions().timeZone

    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default api
