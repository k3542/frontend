import React from 'react'
import { Button } from '@chakra-ui/react'

const Home = () => {
  return (
    <div className="container max-w-4xl mx-auto">
      <h1 className="text-3xl font-bold">Hello world!</h1>
      <Button colorScheme="cyan">Button</Button>
    </div>
  )
}

export default Home
