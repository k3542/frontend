import React from 'react'
import { Button } from '@chakra-ui/react'

const CustomButton = () => {
  return <Button colorScheme="cyan">Button</Button>
}

export default CustomButton
